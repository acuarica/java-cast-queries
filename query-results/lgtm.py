#!/usr/bin/python

import requests
import argparse
import sys
import json
from pprint import pprint

JSESSIONID = "ymSF5y1mn0HsGzq1JgP_Ww"
lgtm_short_session="640550027dbe5bdb58e57e5ca909ff6a2f980d7d405c4b5b6496ff9da4e52a0f7414a0f429d9e0f7fc75c8289c96ab8f73253532c627dcfdc8b3faa6fcff1485"
lgtm_long_session="9c92483678356c331d074890cff83cbee798ac2b8964a64745ec5a4407de3c36ee2874539572b8a2cc4796757d32e69e41683a6e3b41e890e16145998a127da3"
nonce = "6b5ad4e2de6c9cbfd48fb83161f358dea328ad4d9ae438b95e8dbfa89f640a4ccad4af05b9fb6cc14f123c472a9001fc670432885fafe9ddfeba93dad1336f4b"
apiVersion = "3b7bf8713a07f49f4d37c41dc83d00e24ed13f37"

projectKeys="[1878170062]"

# cookies = {'JSESSIONID': JSESSIONID, 'lgtm_long_session': lgtm_long_session}
cookies = {'lgtm_short_session': lgtm_short_session, 'lgtm_long_session': lgtm_long_session}

customQueryKey=1506231257083
nonce="f2097909b1ef27a3536edfbbc1838cc8e1489bf7971c737e9e7bb61d319ae155e7c628cee87cc4176ab2436a46d543f7464e8b5798781998cc0ac50a6b2647cb"
apiVersion="47f13c5e4c7e0265cab2d885ebac1c14dda89fbd"

def getCustomQueryByKey():
    r = requests.get("https://lgtm.com/internal_api/v0.2/getCustomQueryByKey",
        params={
            'customQueryKey': customQueryKey,
            'nonce': nonce,
            'apiVersion': apiVersion
        }, cookies=cookies)
    return r

# r = getCustomQueryByKey()
# print r
# outjson = json.dumps(json.loads(r.text), indent=2)
# with open("query.json", "w") as outfile:
#     outfile.write(outjson)

def runQuery(filename):
    print >> sys.stderr, "[Running Query '%s' ... ]" % filename

    with open(filename) as f:
        ql = f.read()

    r = requests.post("https://lgtm.com/internal_api/v0.2/runQuery",
                  data={
                          'lang': 'java',
                          'projectKeys': projectKeys,
                          'queryString': ql,
                          'guessedLocation': "",
                          'nonce': nonce,
                          'apiVersion': apiVersion
                      },
                      cookies=cookies)
    return r

def getCustomQueryRunResults(filename):
    print >> sys.stderr, "[Getting Query Run Results from '%s' ... ]" % filename

    with open(filename) as f:
        run = f.read()

    js = json.loads(run)
    key = js['data']['runs'][0]['key']
    print >> sys.stderr, "[Query Run Results Key '%s' ... ]" % key

    r = requests.get("https://lgtm.com/internal_api/v0.2/getCustomQueryRunResults", params = {
        'startIndex': 0,
        'count': 3,
        'unfiltered': 'false',
        'queryRunKey': key,
        'nonce': nonce,
        'apiVersion': apiVersion
    }, cookies=cookies)
    return r

if __name__ == '__main__2':
    parser = argparse.ArgumentParser(description='Run QL query.')
    parser.add_argument('action', choices=['run', 'results'])
    parser.add_argument('infile', metavar='infile', help='Input file to run')
    parser.add_argument('outfile', metavar='outfile', help='Output file to run')
    args = parser.parse_args()

    if args.action == 'run':
        r = runQuery(args.infile)
    elif args.action == 'results':
        r = getCustomQueryRunResults(args.infile)

    print >> sys.stderr, "[Status Code: %s (%s)]" % (r.status_code, r.reason)

    if r.status_code == 200:
        outjson = json.dumps(json.loads(r.text), indent=2)
        with open(args.outfile, "w") as outfile:
            outfile.write(outjson)
    else:
        raise Warning(r, r.text)



# sk = getSnapshotKey(1879340034)
# print sk