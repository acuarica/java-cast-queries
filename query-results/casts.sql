
select file, s, t, 1,0,0,0        from casts where pattern = 'Cast' group by file, s, t union
select file, s, t, 0,1,0,0        from casts where pattern = 'RefCast' group by file, s, t union
select file, s, t, 0,0,1,0        from casts where pattern = 'PrimCast' group by file, s, t union
select file, s, t, 0,0,0,count(*) from casts where pattern not in ('Cast','RefCast','PrimCast') group by file, s, t
;

select count(*)
from casts
where pattern = 'Cast'
;

select count(*)
from casts
where pattern = 'RefCast'
;

select count(*)
from casts
where pattern = 'PrimCast'
;