
install.packages("reshape2")
install.packages("ggplot2")

library("DBI")
library(RSQLite)
library(reshape2)
library(ggplot2)

#dbDisconnect(con)
con = dbConnect(SQLite(), dbname="output.sqlite3")

reposnocasts = dbGetQuery(con, "select * from repos where repoid not in (select distinct repoid from casts)")

reposnolink = dbGetQuery(con, "select repoid, count(*) from casts where link='' group by repoid")

casts = dbGetQuery(con, "select * from casts where link!=''")
nrow(casts)
length(unique(casts$repoid))
#casts = dbReadTable(con, "casts")

df = casts
df$file <- NULL

takeSample <- function(n) {
  s = df[sample(nrow(df), n), ]
  write.csv(s,sprintf('casts-%s.csv', n))
}

#takeSample(50)
#takeSample(100)
#takeSample(500)
#takeSample(1000)
#takeSample(5000)
#takeSample(10000)

#takeSample(481)
#takeSample(47)
#takeSample(3)

df.wide = dcast(casts, repoid~pattern, length, value.var="pattern")
df.count = dcast(casts, repoid+file+s+t ~ "count", length, value.var="pattern")
df.count = dcast(df.count, repoid~"count", length, value.var="count")
#df.count$count == df.wide$Cast

df = df.wide
#df = merge(df.count, df.wide)
#df$Cast <- NULL
df$repoid <- as.factor(df$repoid)
df.long = melt(df, id.vars=c("repoid", "Cast"), variable.name="pattern")
df.long$value = df.long$value / df.long$Cast

ggplot(df.long, aes(x = repoid, y = pattern)) + 
  geom_tile(aes(fill=value)) + 
  scale_fill_gradient(low="white", high="red")
ggsave('heatmatrix-50.png')

df.count = dcast(casts, repoid+file+s+t ~ "count", length, value.var="pattern")
assertthat::are_equal(nrow(df.count[df.count$count < 2, ]), 0)

df.wide = dcast(casts, repoid+file+s+t~pattern, length, value.var="pattern")
assertthat::are_equal(nrow(df.wide[df.wide$Cast != 1, ]), 0)
#df.wide[df.wide$Cast != 1, ]

df = merge(df.count, df.wide)

df.sum = dcast(df, repoid+file+s+t ~ "count", length, value.var="pattern")

#df = df.wide

nrow(casts)
nrow(df)
nrow(df[df$Cast == 1, ])
nrow(df[df$count < 2, ]) 
nrow(df[df$RefCast == 1, ]) + nrow(df[df$PrimCast == 1, ])
nrow(df[df$count == 2 && df$RefCast == 1, ])


install.packages("httr")
install.packages("rjson")

library(httr)
library(rjson)

JSESSIONID = "0g9UouKXmt-f0TpZq13q-Q"
lgtm_long_session = "beb1efef07497b6f0e04b86ecc3af5aca957487080a943ec06319257c09d81329d6e6e3255872df5510f1a0c7b50f701cfaf041029f11e736b3ea36a23af9ce0"
nonce = "54175e93002c2a4605d65976747fe4a6bd619fcbfb238fe2d69aecacaf6c0ea78e9ea1e21fadbbdd5d83c966612efd7c2a61219ad0c1e1de9fa1ff7a78942e61"
apiVersion = "5af1021dae758c5a4b40cce5a121903bf968ad2c"

projectKeys="[1878170062]"

c = set_cookies("JSESSIONID"=JSESSIONID, "lgtm_long_session"=lgtm_long_session)
q = list(
  "startIndex"=0,
  "count"=3,
  "unfiltered"="false",
  "queryRunKey"=1506267945995,
  "nonce"=nonce,
  "apiVersion"=apiVersion)

r <- GET("https://lgtm.com/internal_api/v0.2/getCustomQueryRunResults", query=q,c)

content(r, "text")

json <- fromJSON(file="../hola.json")
