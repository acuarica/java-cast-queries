
package ch.usi.inf.sape.cast;

public class Interface {
    C c;

    void f(boolean b) {
        if (b) {
            c = new D();
        } else {
            c = new Eclass();
        }
        c.m();
    }

}

interface I {
    void m();
}

abstract class C implements I {
}

class D extends C {
    public void m() {
    }
}

class Eclass extends C {
    public void m() {
    }
}
