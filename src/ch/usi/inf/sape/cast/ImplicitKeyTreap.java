package ch.usi.inf.sape.cast;

public class ImplicitKeyTreap<T> {

	protected Node<T> root = null;

	public int getIndexByValue(T value) {
//		final Node<T> node = (Node<T>) root;
		final Node<T> node = root;
		if (value == null || node == null)
			return Integer.MIN_VALUE;
//		final Node<T> l = (Node<T>) node.left;
		final Node<T> l = node.left;

		final int leftSize = ((l != null) ? l.size : 0);
		final int idx = leftSize;
		if (value.equals(node.value))
			return idx;

		return 0;
	}

	public static class Node<T> {
		private T value = null;
		private int size;
		private Node<T> left = null;
	}

}
