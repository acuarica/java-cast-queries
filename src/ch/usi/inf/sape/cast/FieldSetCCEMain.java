package ch.usi.inf.sape.cast;

import java.lang.reflect.Field;

public class FieldSetCCEMain {

	public static void main(String[] args)
			throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException {
		FieldSetCCE fs = new FieldSetCCE();

		Field field = FieldSetCCE.class.getDeclaredField("intField");
		field.setAccessible(true);
		field.set(fs, "Ciao");

		System.out.println(field.get(fs));
	}

}

class FieldSetCCE {

	private Integer intField;

}