package ch.usi.inf.sape.cast;

import java.util.*;

class Casts<T> {
	List<T> ts;
	List<?> ws;
	List<Object> os;
	List<Number> ns;
	List<? extends T> txs;
	List<? extends Object> oxs;
	List<? extends Number> nxs;
	List<? super T> tys;
	List<? super Object> oys;
	List<? super Number> nys;
	List ls;

	<S> S b(S v, S... values) {
		return v;
	}

	void a(Iterable<Object> mock) {
		b("", null);

	}

	<S> S when(S t) {
		return null;
	}

	void m() {
		String a = when(null).toString();

		a((List) Arrays.asList(new Object(), 2));

		ts = (List<T>) ts;
		ts = (List<T>) ws;
		//
		// ts = (List<T>) os;
		// ???
		// ts = os;
		ts = (List<T>) ns;
		ts = (List<T>) txs;
		ts = (List<T>) oxs;
		ts = (List<T>) nxs;
		ts = (List<T>) tys;
//    ts = (List<T>) oys;
//    ts = (List<T>) nys;
		ts = (List<T>) ls;

		ws = (List<?>) ts;
		ws = (List<?>) ws;
		ws = (List<?>) os;
		ws = (List<?>) ns;
		ws = (List<?>) txs;
		ws = (List<?>) oxs;
		ws = (List<?>) nxs;
		ws = (List<?>) tys;
		ws = (List<?>) oys;
		ws = (List<?>) nys;
		ws = (List<?>) ls;

		os = (List<Object>) ts;
		os = (List<Object>) ws;
		os = (List<Object>) os;
		// os = (List<Object>) ns;
		os = (List<Object>) txs;
		os = (List<Object>) oxs;
		// os = (List<Object>) nxs;
		os = (List<Object>) tys;
		os = (List<Object>) oys;
		os = (List<Object>) nys;
		os = (List<Object>) ls;

		ns = (List<Number>) ts;
		ns = (List<Number>) ws;
		// ns = (List<Number>) os;
		ns = (List<Number>) ns;
		ns = (List<Number>) txs;
		ns = (List<Number>) oxs;
		ns = (List<Number>) nxs;
		ns = (List<Number>) tys;
		// ns = (List<Number>) oys;
		ns = (List<Number>) nys;
		ns = (List<Number>) ls;

		txs = (List<? extends T>) ts;
		txs = (List<? extends T>) ws;
		txs = (List<? extends T>) os;
		txs = (List<? extends T>) ns;
		txs = (List<? extends T>) txs;
		txs = (List<? extends T>) oxs;
		txs = (List<? extends T>) nxs;
		txs = (List<? extends T>) tys;
		txs = (List<? extends T>) oys;
		txs = (List<? extends T>) nys;
		txs = (List<? extends T>) ls;

		oxs = (List<? extends Object>) ts;
		oxs = (List<? extends Object>) ws;
		oxs = (List<? extends Object>) os;
		oxs = (List<? extends Object>) ns;
		oxs = (List<? extends Object>) txs;
		oxs = (List<? extends Object>) oxs;
		oxs = (List<? extends Object>) nxs;
		oxs = (List<? extends Object>) tys;
		oxs = (List<? extends Object>) oys;
		oxs = (List<? extends Object>) nys;
		oxs = (List<? extends Object>) ls;

		nxs = (List<? extends Number>) ts;
		nxs = (List<? extends Number>) ws;
		// nxs = (List<? extends Number>) os;
		nxs = (List<? extends Number>) ns;
		nxs = (List<? extends Number>) txs;
		nxs = (List<? extends Number>) oxs;
		nxs = (List<? extends Number>) nxs;
		nxs = (List<? extends Number>) tys;
		// nxs = (List<? extends Number>) oys;
		nxs = (List<? extends Number>) nys;
		nxs = (List<? extends Number>) ls;

		tys = (List<? super T>) ts;
		tys = (List<? super T>) ws;
		tys = (List<? super T>) os;
		tys = (List<? super T>) ns;
		tys = (List<? super T>) txs;
		tys = (List<? super T>) oxs;
		tys = (List<? super T>) nxs;
		tys = (List<? super T>) tys;
		tys = (List<? super T>) oys;
		tys = (List<? super T>) nys;
		tys = (List<? super T>) ls;

//    oys = (List<? super Object>) ts;
		oys = (List<? super Object>) ws;
		oys = (List<? super Object>) os;
		// oys = (List<? super Object>) ns;
		oys = (List<? super Object>) txs;
		oys = (List<? super Object>) oxs;
		// oys = (List<? super Object>) nxs;
		oys = (List<? super Object>) tys;
		oys = (List<? super Object>) oys;
		oys = (List<? super Object>) nys;
		oys = (List<? super Object>) ls;

		// nys = (List<? super Number>) ts;
		nys = (List<? super Number>) ws;
		nys = (List<? super Number>) os;
		nys = (List<? super Number>) ns;
		nys = (List<? super Number>) txs;
		nys = (List<? super Number>) oxs;
		nys = (List<? super Number>) nxs;
		nys = (List<? super Number>) tys;
		nys = (List<? super Number>) oys;
		nys = (List<? super Number>) nys;
		nys = (List<? super Number>) ls;

		ls = (List) ts;
		ls = (List) ws;
		ls = (List) os;
		ls = (List) ns;
		ls = (List) txs;
		ls = (List) oxs;
		ls = (List) nxs;
		ls = (List) tys;
		ls = (List) oys;
		ls = (List) nys;
		ls = (List) ls;

		int n = (int) new Integer(2);
	}
}

