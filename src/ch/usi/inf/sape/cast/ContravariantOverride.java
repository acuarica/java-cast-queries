package ch.usi.inf.sape.cast;

public class ContravariantOverride {

}

class XA {
	public void m(Integer x) {}
}

class XB extends XA {
	
	@Override
	public void m(Integer x) {
		mn(x);
	}

	public void mn(Number x) {}
	
}
