package ch.usi.inf.sape.cast;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

public class Generics {

	void f(List<String> ls) {
		ls.contains(123);
		
	}

	void multi(int th, int tw) {
//        List<byte[]>[][] partialResults2 = new List[th][tw];
        List<?>[][] partialResults = new List[th][tw];
        List<?> a = partialResults[0][0];
        System.out.println(a);
	}

	void a() {
		List<? extends Number> ls = null;
		((List) ls).add(3);
		// Number n = ((List)ls).get(0);
		
		
		List<Integer> ls0 = Collections.singletonList(new Integer(3));
		List<Number> ls1 = Collections.singletonList((Number)new Integer(3));

		List<Integer> ls2 = Arrays.asList(new Integer(3));
		List<Number> ls3 = Arrays.asList((Number)new Integer(3));
		
		List<Number> ls4 = Collections.<Number>singletonList(new Integer(3));
		List<Number> ls5 = Arrays.<Number>asList(new Integer(3));
	}
	
	@SuppressWarnings("rawtypes")
	<T> Object hola(T t) {
		Comparable c = (Comparable) t;
		return c;

	}

	void m0() throws ClassNotFoundException {
		List<?> ls = new ArrayList<Object>();
		Object o = ls.get(0);

		is((Object) null);
		Class<?> cls = null;
		assertThat(cls, is(Class.forName("long")));

	}

	private static <T> T isNull() {
		return null;
	}

	public void n(String s) {
	}

	public void m() {
		Object o = (Class<List<String>>) (Class<?>) List.class;

		n(isNull());
	}

	public <T> Iterator<T> load() {
		return new Iterator<T>() {

			Iterator<T> it = null;

			@Override
			public boolean hasNext() {
				// TODO Auto-generated method stub
				return false;
			}

			@Override
			public T next() {
				return it.next();
			}

		};

	}

	private static void s() {
		List<? extends Number> ls = new ArrayList<Object>();
		List li0 = (List) ls;
		List<Integer> li1 = (List<Integer>) li0;

		ls.add(new Double(3));
		Number n = ls.get(0);
		li0.add("hi");
		ls.get(1);
		li1.get(1);
	}

	void s0() {
		ArrayList<String> ss = new ArrayList<String>();
		String[] sa = ss.toArray();

	}

	public static class MapEntry<K, V> {
		K key;
		V value;

		interface Type<RT, KT, VT> {
			RT get(MapEntry<KT, VT> entry);
		}
	}

	public static class C<T, KT, VT> {

		MapEntry.Type<T, KT, VT> type;

		public T next() {
			return type.get(null);
		}

	}

	public static class DefaultGroovyMethods {

		public static <T> T max(Iterator<T> self, Comparator<T> comparator) {
			return max((Iterable<T>) toList(self), comparator);
			// return max(toList(self), comparator);
		}

		public static <T> T max(Iterable<T> self, Comparator<T> comparator) {
			T answer = null;
			boolean first = true;
			for (T value : self) {
				if (first) {
					first = false;
					answer = value;
				} else if (comparator.compare(value, answer) > 0) {
					answer = value;
				}
			}
			return answer;
		}

		@Deprecated
		public static <T> T max(Collection<T> self, Comparator<T> comparator) {
			return max((Iterable<T>) self, comparator);
		}

		public static <T> List<T> toList(Iterator<T> self) {
			List<T> answer = new ArrayList<T>();
			while (self.hasNext()) {
				answer.add(self.next());
			}
			return answer;
		}

	}

}

class ToArray<T> {

	ArrayList<T> as;

	T[] toArray() {
		Object[] ts = new Object[10];
		ts[0] = as.get(0);
		return ts;
	}

}

class SortedArrayList<E> extends ArrayList<E> {
	private static final long serialVersionUID = 1L;

	protected int compare(final Object k1, final E k2) {
		return ((Comparable<E>) k1).compareTo(k2);
	}
}

class SortedArrayList2<E extends Comparable<E>> extends ArrayList<E> {
	private static final long serialVersionUID = 1L;

	protected int compare(final E k1, final E k2) {
		return k1.compareTo(k2);
	}
}