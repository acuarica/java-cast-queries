package ch.usi.inf.sape.cast;

import java.io.FileNotFoundException;

public class TypecaseRethrow {

	private static void raise() throws Throwable {
		throw (Exception) new FileNotFoundException();
	}

	public static void main(String[] args) {
		try {
			raise(); 
		} catch(FileNotFoundException e) {
			System.out.print("FileNotFound");
			System.out.println(e);
		} catch(Exception e) { 
			System.out.print("Exception");
			System.out.println(e);
		} catch(Throwable e) {
			System.out.print("Throwable");
			System.out.println(e);
		}
	}

}
