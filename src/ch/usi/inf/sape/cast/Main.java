
package ch.usi.inf.sape.cast;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;

import com.sun.corba.se.impl.orbutil.GetPropertyAction;

import sun.misc.Unsafe;

class Literal2 {
	
	void ct() {
//		Object a = (String)new Integer(1);
		
	}
	
	public void m(Short s) {
	}

	public void n() {
		short s;
		s = 485; // OK
		m(s);

//		m(485); // The method m(Short) in the type Literal is not applicable for the arguments (int)

		// byte b = 128; // Type mismatch: cannot convert from int to byte
		// byte[] bs = new byte[] {0, -128, 127, 128}; // Type mismatch: cannot convert from int to byte
		
		byte a0 = 0x28;
		byte a1 = (byte)0x28;
		
		byte[] bs0 = new byte[] {(byte) 0x31 };
		byte[] bs1 = new byte[] { 0x31, 0x17, (byte) 0xb8, 127 };
	}

}

class RedundantNull {
	
	public java.util.Map<java.lang.String, java.lang.String> getHdfsConfig() {
		return (java.util.Map<java.lang.String, java.lang.String>) null;
	}

	public java.util.Map<java.lang.String, java.lang.String> getHdfsConfig2() {
		return null;
	}

	public <K, V> void m() {
		
        Map<K,V>[] ss0 = (Map<K,V>[])new Map<?,?>[10];
        
        // Cannot create a generic array of Map<K,V>
        // Map<K,V>[] ss1 = (Map<K,V>[])new Map<K,V>[10];
        
        Map<K,V> ss2 = (Map<K,V>)new HashMap<K,V>();
	
	}
	
	public <T extends Number> List<T> f(Set<T> set) {
		return null;	
	}

    //protected <T extends BaseEntity> EntityList<T> getEntities(String url, Map<String, String> parameters, IFeedHandler<T> feedHandler) throws ClientServicesException {

	public void g() {
		Set<Integer> ss = null;
		List<Integer> ls = f(ss);
		
	}
}

class MakeGeneric {
	<E> void m() {
		List<E> list = (List<E>) new ArrayList<Object>();
		System.out.println(list);
	}
}

class GenericClass<T> {

}

//interface Interface<TypeVar> {

//	TypeVar get();
//}

//class Clazz implements Interface<Object> {
//
//	public Object get() {
//		return new Object();
//	}
//
//}

public class Main {


	public static void f(Long id) {
		
	}

	private static void sel(int x, String ... args) {
		System.out.print(x);
		System.out.print(args);
		if (args != null) {
			System.out.println(": " + args.length);
		} else {
			System.out.println();
		}
	}

	private static void a(String x, Main y, String ... xs) {
		System.out.println("1");
	}

	private static void a(String x, Object ... xs) {
		System.out.println("2");
	}

	private static Main m() {
		Main res = null;
		SortedSet<Main> ss = new TreeSet<Main>();
		// res = (Main) ss.last();
		res = ss.last();
		return res;

	}
	private static <T> GenericClass<T> newGenericClass() {
		return (GenericClass) new GenericClass<Object>();
	}

//	@SuppressWarnings("unchecked")
//	private static <TypeVar> Interface<TypeVar> newC() {
//		return (Interface<TypeVar>) new Clazz();
//	}

	private static void withUnsafe(Unsafe unsafe) {
//        unsafe.arrayBaseOffset(arg0)

    }

//	@SuppressWarnings("unchecked")
	public static <T> T getAdapter(Class<T> clazz) {
		return (T)new Object();
	
	}

	static <T> T getProperty(T defaultValue) {
		return defaultValue;
	}

	public static void main(String[] args) {
		System.out.println(getProperty(5L).getClass());
		System.out.println(getProperty(5).getClass());
	}

	public static void main2(String[] args) {
		Main m = getAdapter(Main.class);
		System.out.println(m);

//		f(7);
		
		sel(1);
		sel(2, null); // warning
		sel(3, (String)null);
		
		byte[] arr = new byte[] { (byte)194, (byte)195, 127, (byte)128, -1 };
		System.out.println(arr);
		for (int i = 0; i < arr.length; i++) {
			System.out.println(i + ": " + arr[i]);	
		}

		a("A", new Main());

		GenericClass<Integer> gc = newGenericClass();
		System.out.println(gc);

//		Interface<Integer> c = newC();
//		Integer i = c.get();
//		System.out.println(i);
	}
}
