package ch.usi.inf.sape.cast;

public class Listeners {
	static <T> void of(CheckedConsumer<? extends Throwable> listener, Throwable failure) {
//		((CheckedConsumer<Throwable>) listener).accept(failure);
		listener.accept(failure);
	}
}

interface CheckedConsumer<T> {
	void accept(T t);
}