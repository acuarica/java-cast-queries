package ch.usi.inf.sape.cast;

import java.security.AccessController;
import java.security.PrivilegedAction;

public class RawTypes {

	  public ClassLoader getSystemClassLoader0() {
	        return (ClassLoader)
	            AccessController.doPrivileged(new PrivilegedAction() {
	                public Object run() {
	                    ClassLoader cl = null;
	                    try {
	                        cl = ClassLoader.getSystemClassLoader();
	                    } catch (SecurityException ex) {}
	                    return cl;
	                }
	            });
	    }

	  public ClassLoader getSystemClassLoader1() {
	        return 
	            AccessController.doPrivileged(new PrivilegedAction<ClassLoader>() {
	                public ClassLoader run() {
	                    ClassLoader cl = null;
	                    try {
	                        cl = ClassLoader.getSystemClassLoader();
	                    } catch (SecurityException ex) {}
	                    return cl;
	                }
	            });
	    }
}
