package ch.usi.inf.sape.cast;

public class Lambda {

    TransactionTemplate transactionTemplate;

	void test() {
		   transactionTemplate.execute((TransactionCallback<Void>) transactionStatus -> {
               return null;
           });

		   transactionTemplate.execute(transactionStatus -> {
               return null;
           });
	}
}

interface TransactionStatus {} 

@FunctionalInterface
interface TransactionCallback<T> {	
	T doInTransaction(TransactionStatus status);
}

class TransactionTemplate {
	<T> T execute(TransactionCallback<T> action) {
		return null;	
	}
}