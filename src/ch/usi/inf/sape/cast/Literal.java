package ch.usi.inf.sape.cast;

import java.util.Arrays;

public class Literal {
	
	public static void m() {
		byte[] bs = new byte[] {(byte) 0x02, (byte) 0x157 };

		byte[] bs0 = new byte[] {(byte) 0x02, (byte) 0x157, 127 };
	}

	private static void m0(int x, int y) {
		
	}

	private static void m0(Object x, Object y) {
		
	}
	
	private static void a() {
		m0((int)new Integer(1), 2);
	}
	
	private void lit() {
	    final byte[] CHARS = new byte[1 << 16];
        Arrays.fill(CHARS, 3002, 3006, (byte) 33 ); // Fill 4 of value (byte) 33
        Arrays.fill(CHARS, 3002, 3006, 33); // Fill 4 of value (byte) 33
	}
}
