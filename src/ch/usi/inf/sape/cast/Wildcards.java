package ch.usi.inf.sape.cast;

import static org.hamcrest.core.Is.is;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

class PluginFactory {

}

public class Wildcards {

	@Test
	public void m1() throws ClassNotFoundException, InstantiationException, IllegalAccessException {
		Class c = Class.forName("java.lang.String");
		// Type mismatch: cannot convert from Object to PluginFactory
		String pf = (String) c.newInstance();
//		System.out.println(pf.isEmpty());
	}

	@Test
	public void m2() throws ClassNotFoundException, InstantiationException, IllegalAccessException {
		Class<?> c = Class.forName("java.lang.String");
		// Type mismatch: cannot convert from capture#3-of ? to PluginFactory
		String pf = (String) c.newInstance();
//		System.out.println(pf.isEmpty());
	}

	@Test
	public void m3() throws ClassNotFoundException, InstantiationException, IllegalAccessException {
		@SuppressWarnings("unchecked")
		// Type mismatch: cannot convert from Class<capture#4-of ?> to
		// Class<PluginFactory>
		Class<String> c = (Class<String>) Class.forName("java.lang.String");
		String pf = c.newInstance();
//		System.out.println(pf.isEmpty());
	}

}