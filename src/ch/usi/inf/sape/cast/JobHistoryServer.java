package ch.usi.inf.sape.cast;

public class JobHistoryServer {
	protected HistoryContext historyContext;
	private JobHistory jobHistoryService;

	protected void serviceInit() {
		jobHistoryService = new JobHistory();
		historyContext = (HistoryContext) jobHistoryService;
		historyContext = jobHistoryService;
	}
}

class JobHistory extends AbstractService implements HistoryContext {
}

class AbstractService {
}

interface HistoryContext {
}