package ch.usi.inf.sape.cast;

public class Instanceof {

	public static void main(String[] args) {
		Object x = new Integer(1);
		System.out.println((double)x);

		Object a = new A();
		
		System.out.println(a instanceof Object);
		System.out.println(a instanceof A);
		System.out.println(a instanceof B);
	}

}

class A {}

class B extends A {}