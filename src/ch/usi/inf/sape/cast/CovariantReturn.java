
package ch.usi.inf.sape.cast;

import java.awt.Component;

import javax.swing.JFrame;

public class CovariantReturn {

	public int field;

	CovariantReturn(int field) {
		this.field = field;
	}

	@Override
	protected CovariantReturn clone() throws CloneNotSupportedException {
		return new CovariantReturn(this.field);
	}

	@Override
	public String toString() {
		return "{" + field + "}";
	}

	public static void a(Comparable c) {
		System.out.println(c);
	}

	public static void main(String[] args) throws CloneNotSupportedException {
		a((Comparable)(Number)4);

		Comparable<Number> c = (Comparable<Number>) (Number) 4;
		System.out.println(c);
		
		CovariantReturn x = new CovariantReturn(4);
		CovariantReturn y = x.clone();

		System.out.println(x);
		System.out.println(y);
	}

}

class ModelerFrame extends JFrame {
	private static final long serialVersionUID = 1L;
}

abstract class Controller {
    public abstract Component getView();
}

class ModelerController extends Controller {

    protected ModelerFrame frame;
	
//	@Override
	public Component getView1() {
		return frame;
	}

	@Override
	public ModelerFrame getView() {
		return frame;
	}
}