package ch.usi.inf.sape.cast;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

import java.io.Serializable;
import java.util.RandomAccess;

import static org.hamcrest.CoreMatchers.*;

import org.hamcrest.Matcher;
import org.junit.Test;

public class SelectOverload {

	@Test
	public void m() {
		Long l = 4L;
		assertEquals(4L, (long)l);
		
	}
	
	private static byte getSpacing() {
		return 1;
	}

	public static void ae(byte e, byte a) {}
	public static void ae(long e, long a) {}
	public static void ae(int e, int a) {}
	public static void ae(Object e, Object a) {}

	public void n() {
		ae(1, null);
		ae(1, getSpacing());
		ae((byte) 0x1, getSpacing());

		assertThat("", equalTo(""));

		assertThat("", equalTo(3));
		
		
		Class<?>[] interfaces = null;
        assertThat(interfaces.length, is(2));
        assertThat(interfaces, hasItemInArray((Object) Serializable.class));
        assertThat(interfaces, hasItemInArray((Object) RandomAccess.class));

        assertThat(interfaces, hasItemInArray(RandomAccess.class));
	}
	
	private static <T> Matcher<T[]> hasItemInArray(T element) {
		return null;
	}

}
