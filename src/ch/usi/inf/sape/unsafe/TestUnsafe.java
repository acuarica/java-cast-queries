package ch.usi.inf.sape.unsafe;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;

import sun.misc.Unsafe;

public class TestUnsafe {

	private static Unsafe getUnsafe() throws IllegalArgumentException, IllegalAccessException {
        Class<Unsafe> type = Unsafe.class;
        Field[] fields = type.getDeclaredFields();
        for ( Field field : fields )
        {
            if ( Modifier.isStatic( field.getModifiers() )
                 && type.isAssignableFrom( field.getType() ) )
            {
                field.setAccessible( true );
                return type.cast( field.get( null ) );
            }
        }
        return null;
	}

	public static void main(String[] args) throws InstantiationException, IllegalArgumentException, IllegalAccessException {
		Double d = null;
		Number x = (double)(Double)null;

		System.out.println(x);

		Unsafe unsafe = getUnsafe();
		Object o = unsafe.allocateInstance(TestUnsafe.class);
		System.out.println(o);
	}
}
