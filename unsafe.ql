import java

class Unsafe extends Class {
	Unsafe() { hasQualifiedName("sun.misc", "Unsafe") }
}

class UnsafeMethodAccess extends MethodAccess {
	UnsafeMethodAccess() { getQualifier().getType() instanceof Unsafe }
}

from UnsafeMethodAccess uma
select uma
