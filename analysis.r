
# library(tools)

# install.packages("UpSetR")
# install.packages("dplyr")
# install.packages("tidyr")
# install.packages("readr")

library(tidyr)
library(reshape2)
library(UpSetR)
library(DBI)
library(RSQLite)
library(ggplot2)

library(readr) # write_lines
library(jsonlite) # toJSON

dbReadObs <- function(dbPath) {
  print(sprintf("[Reading SQLite DB '%s']", dbPath))
  dbReadTable(dbConnect(SQLite(), dbname=dbPath), "obs")
}

writeDef <- function(defPath, values) {
  print(sprintf("[Writing DEF '%s']", defPath))
  f <- file(defPath)
  writeLines(values, f)
  close(f)
}

stats.raw <- dbReadObs(".lgtm/current-stats/.output.sqlite3")

stats.raw$value <- as.numeric(stats.raw$value)
stats.project <- dcast(stats.raw, project~stat)
stats.project$ratio <- stats.project$MethodWithCast/stats.project$Method

pdf('analysis/stats-methodwcastXproject.pdf', height = 2)
ggplot(stats.project, aes(x="", y=ratio))+
  geom_boxplot()+
  geom_jitter()+
  coord_flip()+
  theme_minimal()+
  theme(axis.title.y=element_blank(),axis.text.y=element_blank(),axis.ticks.y=element_blank())+
  labs(y = "Methods with casts over total number of methods")
dev.off()

values <- c(
  sprintf("\\newcommand{\\nproject}{%s}", format(nrow(stats.project), big.mark=',')),
  sprintf("\\newcommand{\\nloc}{%s}", format(sum(stats.project$LOC), big.mark=',')),
  sprintf("\\newcommand{\\nexpr}{%s}", format(sum(stats.project$Expr), big.mark=',')),
  sprintf("\\newcommand{\\nstmt}{%s}", format(sum(stats.project$Stmt), big.mark=',')),
  sprintf("\\newcommand{\\nmethod}{%s}", format(sum(stats.project$Method), big.mark=',')),
  sprintf("\\newcommand{\\nmethodwithcast}{%s}", format(sum(stats.project$MethodWithCast), big.mark=',')),
  sprintf("\\newcommand{\\castpercentage}{%#.2f}", mean(stats.project$ratio, na.rm=TRUE)*100)
)

writeDef('analysis/stats.def', values)

obs.raw <- dbReadObs(".lgtm/current-obs/.output.sqlite3")
# Make unique id
obs.raw$id = rownames(obs.raw)

obs.categories <- dcast(obs.raw, category~"total", length, value.var="id")
obs.categories$format <- sprintf("\\newcommand{\\n%s}{%s}", obs.categories$category, format(obs.categories$total, big.mark=','))

writeDef('analysis/categories.def', obs.categories$format)

eqm.raw <- obs.raw[obs.raw$category=='EqualsMethod', ]
eqm.raw$category <- NULL

casts.raw <- obs.raw[obs.raw$category %in% c('CastExpr', 'CastMethod'), ]
casts.raw$category <- NULL
casts.raw[casts.raw$patterns=="",]$patterns <- "Any"
casts.raw[casts.raw$tags=="",]$tags <- "Any"

casts.patterns.long <- separate_rows(casts.raw, patterns, sep='\\|')
casts.patterns.wide <- dcast(casts.patterns.long, id+obs~patterns, length, value.var="patterns")
casts.patterns.total <- dcast(casts.patterns.long, patterns~"total", length, value.var="patterns")

attach(casts.patterns.total)
casts.patterns.bytotal <- casts.patterns.total[order(total),] 
casts.patterns.long$patterns <- factor(casts.patterns.long$patterns, levels = casts.patterns.bytotal$patterns)

pdf('analysis/table-patterns.pdf')
ggplot(casts.patterns.long, aes(x=patterns))+
  geom_bar()+
  geom_text(stat='count', aes(label=..count..,y=..count..+30))+
  coord_flip()+
  theme_minimal()+
  labs(x="Cast Patterns", y = "# Instances")
dev.off()

pdf('analysis/upset-patterns.pdf')
upset(casts.patterns.wide,nsets=ncol(casts.patterns.wide),nintersects=NA,mb.ratio = c(0.3, 0.7))
dev.off()

casts.tags.long <- separate_rows(casts.raw, tags, sep='\\|')
casts.tags.wide <- dcast(casts.tags.long, id+obs~tags, length, value.var="tags")

pdf('analysis/upset-tags.pdf')
upset(casts.tags.wide,nsets=ncol(casts.tags.wide),mb.ratio = c(0.2, 0.8))
dev.off()

casts.tags.table <- dcast(casts.tags.long, tags~"count", length, value.var="obs")
casts.tags.table %>% toJSON() %>% write_lines('analysis/tags.json')

casts.long <- separate_rows(casts.raw, patterns, sep='\\|')
casts.long <- separate_rows(casts.long, tags, sep='\\|')

for (pattern in levels(as.factor(casts.patterns.long$patterns))) {
  upsetTagsXPattern <- sprintf('analysis/upset-tags-%s.pdf', pattern)
  print(upsetTagsXPattern)
  casts.tagsXPattern <- dcast(casts.long[casts.long$patterns==pattern,], id+obs~tags, length, value.var="tags")
  
  pdf(upsetTagsXPattern)
  upset(casts.tagsXPattern,nsets=ncol(casts.tagsXPattern),mb.ratio = c(0.2, 0.8))
  dev.off()
}


# casts.delim <- (function(casts.noicon) {
#   casts.noicon$tags <- paste('|', casts.noicon$tags, '|', sep='')
#   casts.noicon
# })(casts.noicon)
# 
# casts.expand <- (function(casts.delim) {
#   tags.expanded <- read.csv(file='tagsExpanded.csv', header=TRUE, strip.white = TRUE)
#   tags.expanded$tag <- paste('|', tags.expanded$tag, '|', sep='')
#   tags.expanded$supertags <- paste('|', tags.expanded$supertags, '|', sep='')
# 
#   expand <- function(tags) {
#     for (t in rownames(tags.expanded)) {
#       tags = gsub(tags.expanded[t, 'tag'], tags.expanded[t, 'supertags'], tags, fixed=TRUE)
#     }
#     tags
#   }
# 
#   casts.delim$tags <- expand(casts.delim$tags)
#   # Strip first&last delim '|' to avoid empty rows when in long format
#   casts.delim$tags <- substring(casts.delim$tags, 2)
#   casts.delim$tags <- substring(casts.delim$tags, 1, nchar(casts.delim$tags) - 1)
#   
#   casts.delim
# })(casts.delim)

ps <- levels(as.factor(casts.patterns.long$patterns))
npattern <- length(ps)

casts.patterns.wide.any <- casts.patterns.wide[casts.patterns.wide$Any != 0, ]
casts.patterns.wide.pat <- casts.patterns.wide[casts.patterns.wide$Any == 0, ]

any.percentage <- nrow(casts.patterns.wide.any)*100/nrow(casts.patterns.wide)
pat.percentage <- nrow(casts.patterns.wide.pat)*100/nrow(casts.patterns.wide)

values <- c(
sprintf("\\newcommand{\\npattern}{%s}", format(npattern, big.mark=',')),
sprintf("\\newcommand{\\anypercentage}{%#.2f}", any.percentage),
sprintf("\\newcommand{\\patpercentage}{%#.2f}", pat.percentage)
)

writeDef('analysis/casts.def', values)
