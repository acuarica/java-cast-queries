/*
 * Provides general statistics about the project.
 * It counts all reference casts and number of expressions.
 * The number of expressions is a way to estimate the size of the project.
 *
 * We have also added total number of statements,
 * number of compilation units (compilable files) and number of lines of code (LOC).
 * LOC is the traditional way of measure how large a project is.
 * In turn, this can be correlated to the number of expressions, to put them in perspective.
 */
import casts
import category

/**
 * Any stat should be `final` and extend this class.
 */
abstract class Stat extends string {
	bindingset[this]
	Stat() { any() }
	abstract string show();
}

/**
 * Total number of compilation units.
 */
final class CUStat extends Stat {
	CUStat() { this = "CU" }
	int getValue() { result = count(CompilationUnit cu | cu.fromSource() ) }
	override string show() { result = getValue().toString() }
}

/**
 * Total number of lines of code (LOC).
 */
final class LOCStat extends Stat {
	LOCStat() { this = "LOC" }
	int getValue() { result = sum(CompilationUnit cu || cu.getNumberOfLinesOfCode()) }
	override string show() { result = getValue().toString() }
}

/**
 * Total number of methods implementations, i.e., with source code.
 */
final class MethodStat extends Stat {
	MethodStat() { this = "Method" }
	int getValue() { result = count(Method m | exists(Block b | m.getBody() = b)) }
	override string show() { result = getValue().toString() }
}

/**
 * Total number of methods that contains at least one type cast expression.
 */
final class MethodWithCastStat extends Stat {
	MethodWithCastStat() { this = "MethodWithCast" }
	int getValue() { result = count(Callable m | exists(Cast ce | ce.getEnclosingCallable() = m)) }
	override string show() { result = getValue().toString() }
}

/**
 * Total number of statements.
 */
final class StmtStat extends Stat {
	StmtStat() { this = "Stmt" }
	int getValue() { result = count(Stmt s) }
	override string show() { result = getValue().toString() }
}

/**
 * Total number of expressions.
 */
final class ExprStat extends Stat {
	ExprStat() { this = "Expr" }
	int getValue() { result = count(Expr e) }
	override string show() { result = getValue().toString() }
}

/**
 * Ratio of how many methods contain at least a cast.
 * That is, number of methods with cast over total number of methods with implementation.
 * This gives an idea of how often casting is used.
 */
final class CastRatioStat extends Stat {
	CastRatioStat() { this = "CastRatio" }
	MethodWithCastStat mwc;
	MethodStat m;
	float getValue() { result = mwc.getValue().(float) * 100 / m.getValue() }
	override string show() { result = getValue() + "%" }
}

/**
 * How many observations per category.
 */
final class CategoryStat extends Stat {
	Category c;
	CategoryStat() { this = c }
	int getValue() { result = count(c.getAnObs()) }
	override string show() { result = getValue().toString() }
}

/**
 * How many casts with patterns there are.
 */
final class CastPatternStat extends Stat {
	CastPatternStat() { this = "CastPattern" }
	int getValue() { result = count(Cast cp) }
	override string show() { result = getValue().toString() }
}

/**
 * Percentage of casts with patterns.
 */
final class CastCoverageStat extends Stat {
	CastCoverageStat() { this = "CastCoverage" }
	float getValue() { result = count(Cast cp).(float) * 100 / count(Cast ce) }
	override string show() { result = getValue() + "%" }
}

from Stat stat
select stat, stat.show() as value, stat.getAQlClass() as qlclass
order by qlclass
