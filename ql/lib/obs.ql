import casts
import category
import show

import semmle.code.java.controlflow.Guards

/**
 * The expression of the cast is a variable which was assigned before in the same method.
 */
//final class DefUseCast extends Cast {
//	DefUseCast() { exists(getDefUse()) }
//}

/**
 * A cast applied to a `new` expression.
 */
final class NewCast extends Cast {
	NewCast() { getExpr() instanceof ClassInstanceExpr }
}

/**
 * A cast used in a \code{throw} statement.
 */
final class ThrowCast extends Cast {
	ThrowCast() { getParent() instanceof ThrowStmt }
}

/**
 * A cast used in a variable declaration is marked as unchecked.
 */
final class UncheckedVarDeclCast extends Cast {
	UncheckedVarDeclCast() { getParent().(LocalVariableDeclExpr).getVariable().getAnAnnotation() instanceof UncheckedAnnotation }
}

/**
 * A cast's method is marked as unchecked.
 */
final class UncheckedCallableCast extends Cast {
	UncheckedCallableCast() { getEnclosingCallable().getAnAnnotation() instanceof UncheckedAnnotation }
}

/**
 * A cast's class is marked as unchecked.
 */
final class UncheckedClassCast extends Cast {
	UncheckedClassCast() { getEnclosingCallable().getDeclaringType().getAnAnnotation() instanceof UncheckedAnnotation }
}

/**
 * This cast is in a test method of a common testing framework (including JUnit and TestNG).
 */
final class InTestCast extends Cast {
	InTestCast() { getEnclosingCallable() instanceof TestMethod }
}
	
/**
 * Cast an expression into `Object`.
 */
final class IntoObjectCast extends Cast {
	IntoObjectCast() { getTargetType() instanceof TypeObject }
}

/**
 * A cast controlled by more the one `instanceof`s.
 */
final class UnionGuardedCast extends Cast {
	UnionGuardedCast() {
		count(ConditionBlock cb, InstanceOfExpr ioe |
			ioe = cb.getCondition() and
			cb.getTestSuccessor(true) = getBasicBlock()
		) > 1
	}
}

/*
 * Query all observations defined.
 * 
 * Columns 'patterns', 'tags', and 'show' need to be concatenated since they might have more than one result.
 * By concatenating the results in each column, we keep one line for observation.
 * 
 * We sort the results by 'obs' to keep the observations in the same order across runs.
 * This allows us to easily identify observations every time we re-run the query after adding either a tag or a pattern.
 */
from Category category, Top obs//, string c
where obs = category.getAnObs() //and c = getAClass(obs, "Cast")
select
	category,
	concat(getAClass(obs, "Cast"), "|") as patterns,
	//concat(obs.(Pattern).getPattern(), "|") as patterns,
//	concat(obs.(Tag).getTag(), "|") as tags,
	concat(obs.(Show).show(), " | ") as show,
	obs,
	getLgtmUrl(obs) as url
order by obs
