/**
 * Language and JDK definitions.
 * Provides more definitions from the JDK.
 * All definitions are taken from Java 8.
 */
import java

/*
 * using ConditionBlock
 */
import semmle.code.java.controlflow.Guards

/*
 * using defUsePair
 */
import semmle.code.java.dataflow.DefUse

/**
 * 
 */
string getLgtmUrl(Top obs) {
	result =
		"https://lgtm.com/projects/g/" +
		"mockito/mockito/snapshot/da68900466a17e21fef3e27690f4cef4b5c240ea"
//		"square/leakcanary/snapshot/390e88a55fc2b334676a960d6fa71d2c7791c526"
		//"square/sqlbrite/snapshot/b3282b34668b7d1aa01b6f4a69b758d9b9eb3994"
		+ "/files/" + obs.getLocation().getFile().getRelativePath() + "?sort=name&dir=ASC&mode=heatmap&showExcluded=false#L" + obs.getLocation().getStartLine()
}

/**
 * Represents the `java.lang.reflect.Array` type.
 * 
 * @see https://docs.oracle.com/javase/8/docs/api/java/lang/reflect/Array.html
 */
final class TypeArray extends Class {
	TypeArray() {
		getSourceDeclaration().hasQualifiedName("java.lang.reflect", "Array")
	}
}

/**
 * The `getClass` method defined in the `java.lang.Object` type.
 * 
 * @see https://docs.oracle.com/javase/8/docs/api/java/lang/Object.html#getClass--
 */
final class GetClassMethod extends Method {
	GetClassMethod() {
		hasName("getClass") and
		getNumberOfParameters() = 0 and
		getReturnType() instanceof TypeClass and
		getDeclaringType() instanceof TypeObject
	}
}

/**
 * The `isArray` method defined in the `java.lang.Class` class.
 * 
 * @see https://docs.oracle.com/javase/8/docs/api/java/lang/Class.html#isArray--
 */
final class IsArrayClassMethod extends Method {
	IsArrayClassMethod() {
		hasName("isArray") and
		getDeclaringType() instanceof TypeClass and
		getNumberOfParameters() = 0 and
		getReturnType().hasName("boolean")
	}
}

/**
 * Represents a method access to the `getClass` method defined in the `java.lang.Object` type.
 * 
 * @see GetClassMethod
 */
final class GetClassMethodAccess extends MethodAccess {
	GetClassMethodAccess() {
		getMethod() instanceof GetClassMethod
	}
}

/**
 * An `unchecked` suppress warning.
 * 
 * @see https://docs.oracle.com/javase/8/docs/api/java/lang/SuppressWarnings.html
 */
final class UncheckedAnnotation extends SuppressWarningsAnnotation {
	UncheckedAnnotation() {
		getASuppressedWarning() = "\"unchecked\""
	}
}

/**
 * For our study, a cast can be either a type cast expression or
 * a call to the `cast` method in the `Class` type.
 * 
 * We also include here all reaching definitions of the cast
 * in case the cast expression is applied to a variable.
 */
class Cast extends CastExpr {
	Type getTargetType() { result = getTypeExpr().getType() }
	Expr getExprOrDef() {
		result = getExpr() or
		exists (VariableAssign def |
			defUsePair(def, getExprOrDef()) and
			result = def.getSource()
		)
	}
}

/**
 * The cast's expression is a variable access.
 */
class VarCast extends Cast {
	Variable var;
	VarCast() { var.getAnAccess() = getExpr() }
	Variable getVar() { result = var }
	
	Expr getADef() {
		exists (VariableAssign def |
			defUsePair(def, getExpr()) and
			result = def.getSource()
		)
	}
	
}

/**
 * Cast's expression is a variable controlled by an `instanceof`.
 */
class ControlByInstanceOfCast extends VarCast {
	InstanceOfExpr iof;
	private ConditionBlock cb;
	ControlByInstanceOfCast() {
		iof = cb.getCondition() and
		cb.controls(getBasicBlock(), true) and
		var.getAnAccess() = iof.getExpr()
	}
	InstanceOfExpr getIof() {
		result = iof
	}
}

/**
 * Cast's expression is a variable guarded by an `instanceof`.
 */
class GuardByInstanceOfCast extends ControlByInstanceOfCast {
	GuardByInstanceOfCast() {
		forall (VariableUpdate def | defUsePair(def, getExpr()) |
		  defUsePair(def, iof.getExpr())
		)
	}
}

/**
 * An `Argument` extended with the parameter that corresponds to.
 */
class ArgumentEx extends Argument {
	Parameter param;
	ArgumentEx() {
		call.getCallee().getParameter(pos) = param
	}
}

/**
 * A vararg argument.
 */
class VarargArgument extends ArgumentEx {
	VarargArgument() {
		isVararg()
	}
}

/**
 * An explicit varargs array argument.
 */
class ExplicitVarargsArrayArgument extends ArgumentEx {
	ExplicitVarargsArrayArgument() {
		isExplicitVarargsArray()
	}
}

/**
 * An overloaded argument to a call site.
 */
class OverloadedArgument extends ArgumentEx {
	Callable target;
	Callable overload;
	OverloadedArgument() {
		target = call.getCallee() and
		overload = target.getDeclaringType().getACallable() and
		overload.getName() = target.getName() and
		target != overload
	}
	Callable getTarget() { result = target }
	Callable getAnOverload() { result = overload }
}

/**
 * This expression occurs in an overriden method, i.e., using the `@Override` annotation.
 */
class InOverrideExpr extends Expr {
	private Method overridenMethod;
	InOverrideExpr() {
		getEnclosingCallable().(Method).overrides(overridenMethod)
	}
	Method getOverridenMethod() { result = overridenMethod }
}

/**
 * An expression inside a `switch` statement.
 */
final class SwitchedExpr extends Expr {
	SwitchStmt switchStmt;
	SwitchedExpr() {
		exists(int n | getEnclosingStmt() = switchStmt.getStmt(n))
	}
	SwitchStmt getSwitchStmt() {
		result = switchStmt
	}
}

/**
 * A generalization of the `instanceof` expression.
 */
class InstanceOf extends Expr {
	private Expr expr;
	InstanceOf() {
		(this.(InstanceOfExpr).getExpr() = expr) or
		exists (Expr e, Expr f |
			this.(AndLogicalExpr).hasOperands(e, f) and
			(e.(InstanceOf).getExpr() = expr or f.(InstanceOf).getExpr() = expr)
		)
	}
	Expr getExpr() { result = expr }
}

predicate isSubtype(RefType sub, RefType sup) {
	sub.getASupertype*() = sup
}

predicate controlByEqualityTest(Expr e, Expr f, Expr c) {
	exists (ConditionBlock cb, EqualityTest eqe |
		eqe.hasOperands(e, f) and eqe = cb.getCondition() and (
			(eqe.getOp() = " == " and cb.controls(c.getBasicBlock(), true)) or
			(eqe.getOp() = " != " and cb.controls(c.getBasicBlock(), false))
		)
	)
}

predicate controlByEqualsMethod(Expr e, Expr f, Expr c) {
	exists (ConditionBlock cb, MethodAccess ema |
		ema.getMethod() instanceof EqualsMethod and (
			(ema.getQualifier() = e and ema.getArgument(0) = f) or
			(ema.getQualifier() = f and ema.getArgument(0) = e)
		) and (
			ema = cb.getCondition() and cb.controls(c.getBasicBlock(), true)
		)
	)
}

/**
 * 
 */
class UpCast extends Cast {
	UpCast() {
		getExpr().getType().(RefType).getASupertype+() = getTargetType()
	}
}

final class NullCast extends Cast {
	NullCast() { getExpr() instanceof NullLiteral }
}

/**
 *
 */
class SelectOverloadCast extends Cast {
	SelectOverloadCast() {
		(this instanceof NullCast or this instanceof UpCast) and
		this instanceof OverloadedArgument
	}
	Callable getOverload() {
		result = this.(OverloadedArgument).getAnOverload()
	}
}

/**
 * 
 */
class CovariantGenericCast extends Cast {
	Argument arg;
	Call call;
	Callable m;
	CovariantGenericCast() {
		this = arg and
		call = arg.getCall() and
		arg.getCall().getCallee() = m and
		(
			m.getReturnType().(ParameterizedType).getATypeArgument() =
					m.getParameterType(arg.getPosition()).(TypeVariable) or
			m.getReturnType().(TypeVariable) =
					m.getParameterType(arg.getPosition()).(TypeVariable)
		)
	}
	Argument getArgument() { result = arg }
	Call getCallSite() { result = call }
	Callable getCallee() { result = m }
}

predicate containsWildcard(Type t) {
	t instanceof Wildcard or
	containsWildcard( t.(ParameterizedType).getATypeArgument() )
}

class VariableSupertypeCast extends VarCast {
	VariableSupertypeCast() {
		forex (Type t | t = getADef().getType() | t = getTargetType()) and
		isSubtype(getTargetType(), getExpr().getType())
	}
}

class AccessSuperclassFieldCast extends Cast {
	FieldAccess fa;
	AccessSuperclassFieldCast() {
		this instanceof UpCast and
		fa.getQualifier().getProperExpr() = this and (
		getExpr().getType().(RefType).declaresField(fa.getField().getName()) or
		( fa.getField().isPrivate() or fa.getField().isProtected() )
		)
	}
}

/**
 * This cast is in a `clone` method.
 */
class CloneCast extends Cast {
	private CloneMethod cm;
	private SuperMethodAccess sma;
	private CloneMethod cma;
	CloneCast() {
		getEnclosingCallable() = cm and
		getExprOrDef() = sma and
		sma.getMethod() = cma
	}
	CloneMethod getEnclosingClone() { result = cm }
	SuperMethodAccess getSuperMethodAccess() { result = sma }
	CloneMethod getCloneCall() { result = cma }
}

/**
 * A cast to a variable controlled by a `isArray` method access.
 * 
 * @see IsArrayClassMethod
 */
class ControlByIsArrayCast extends VarCast {
	ConditionBlock cb;
	MethodAccess iama;
	ControlByIsArrayCast() {
		exists (VariableAssign def, GetClassMethodAccess gcls |
			gcls.getQualifier() = var.getAnAccess() and
			def.getSource() = gcls and
			defUsePair(def, iama.getQualifier().(VarAccess) )
		) and
		iama.getMethod() instanceof IsArrayClassMethod and
		(
			(cb.getCondition() = iama and cb.controls(getBasicBlock(), true)) or
			(cb.getCondition().(LogNotExpr).getExpr() = iama and
				cb.controls(getBasicBlock(), false)
			)
		)
	}
}

/**
 * A covariant return cast.
 * Cast the result of a super call in an overridden method with covariant return.
 * (maybe see also family polymorphism).
 */
class CovariantReturnTypeCast extends Cast {
	Method m;
	MethodAccess ma;
	CovariantReturnTypeCast() {
		getExpr() = ma and ma.isOwnMethodAccess() and
		getEnclosingCallable() = m and m.overrides(ma.getMethod())
	}
}

/**
 * A cast to read objects on type `java.io.ObjectInputStream`.
 */
class DeserializationCast extends Cast {
	DeserializationCast() {
		getExprOrDef().(MethodAccess).getMethod() instanceof ReadObjectMethod
	}
}

/**
 * This cast is guarded by a `java.lang.Class.getClass` test.
 */
class GetClassGuardsVarCast extends Cast {
	GetClassMethodAccess tma;
	GetClassMethodAccess oma;
	GetClassGuardsVarCast() {
		tma.isOwnMethodAccess() and
		oma.getQualifier() = this.(VarCast).getVar().getAnAccess() and
		(
			controlByEqualityTest(tma, oma, this) or
			controlByEqualsMethod(tma, oma, this)
		)
	}
}

/**
 * Provides classes to work with the *AutoValue* library.
 * An `@AutoValue` annotation.
 * 
 * @see https://github.com/google/auto/tree/master/value
 */
class AutoValueAnnotation extends Annotation {
	AutoValueAnnotation() {
		getType().hasQualifiedName("com.google.auto.value", "AutoValue")
	}
}

/**
 * A class annotated with the `@AutoValue` annotation.
 */
class AutoValueClass extends Class {
	AutoValueClass() {
		getAnAnnotation() instanceof AutoValueAnnotation and
		isAbstract()
	}
}

/**
 * An `@AutoValue` generated class extends directly from an `@AutoValue` class.
 * 
 * @see AutoValueClass
 */
class AutoValueGenerated extends Class {
	AutoValueGenerated() {
		count(getASupertype()) = 1 and
		getASupertype() instanceof AutoValueClass
	}
}

/**
 * This cast is in an `equals` method.
 */
class EqualsCast extends VarCast {
	EqualsCast() {
		getVar() instanceof Parameter and
		getEnclosingCallable() instanceof EqualsMethod and
		(
			this instanceof GuardByInstanceOfCast or
			this instanceof GetClassGuardsVarCast
		) and (
			getTargetType() = getEnclosingCallable().getDeclaringType() or
			(
				getTargetType() = getEnclosingCallable().getDeclaringType().
								getASupertype+() and
				getEnclosingCallable().getDeclaringType()
								instanceof AutoValueGenerated 
			)
		)
	}
}

class FluentAPICast extends Cast {
	TypeVariable x;
	GenericType enclosingClass;
	FluentAPICast() {
		getExpr() instanceof ThisAccess and
		getParent() instanceof ReturnStmt and
		x = getTargetType() and
		enclosingClass = getExpr().getType() and
		x.hasTypeBound() and
		x.getFirstTypeBound().getType() = enclosingClass and
		x.getFirstTypeBound().getType().(GenericType).getATypeParameter() = x
	}
}

class OnArrayGenericArrayCast extends Cast {
	OnArrayGenericArrayCast() {
		getTargetType().(Array).getComponentType() instanceof TypeVariable and
		getExpr().getType() instanceof Array
	}
}

class TypeVariableGenericArrayCast extends Cast {
	TypeVariableGenericArrayCast() {
		getExprOrDef() instanceof ArrayAccess and
		getTargetType() instanceof TypeVariable
	}
}

class OnElementGenericArrayCast extends Cast {
	OnElementGenericArrayCast() {
		(
		getExprOrDef().(ArrayAccess).getArray().getType().(Array).getComponentType() instanceof RawType or
		containsWildcard( getExprOrDef().(ArrayAccess).getArray().getType().(Array).getComponentType() )
		) and
		getTargetType() instanceof ParameterizedType
	}
}

class GuardByClassLiteral extends VarCast {
	TypeLiteral tl;
	GetClassMethodAccess gcma;
	GuardByClassLiteral() {
		gcma.getQualifier() = getVar().getAnAccess() and
		isSubtype(tl.getTypeName().getType(), getTargetType()) and (
			controlByEqualityTest(tl, gcma, this) or
			controlByEqualsMethod(tl, gcma, this)
		)
	}
	TypeLiteral getClassLiteral() { result = tl }
	GetClassMethodAccess getGetClass() { result = gcma }
}

/**
 * 
 */ 
class UseRawTypeCast extends Cast {
	MethodAccess ma;
	RawType rt;
	UseRawTypeCast() {
		ma = getExpr() and
		rt = ma.getQualifier().getType() and
		ma.getMethod().getSourceDeclaration().getReturnType()
				instanceof TypeVariable
	}
	MethodAccess getMethodAccess() {
		result = ma
	}
}

/**
 * Holds if this `IfStmt` is a chain of `instanceof` tests.
 * 
 * Based on a lgtm.com rule.
 * @see https://lgtm.com/rules/910065/
 */
class ChainedIofStmt extends IfStmt {
	int n;
	ChainedIofStmt() {
		exists(int rest | (
			if getElse() instanceof IfStmt
			then rest = getElse().(ChainedIofStmt).getNumberOfIof()
			else rest = 0
		) and (
			if getCondition() instanceof InstanceOfExpr
			then n = 1 + rest
			else n = rest
			)
		)
	}
	int getNumberOfIof() {
		result = n
	}
	InstanceOfExpr getIof() {
		result = getCondition()
	}
}

/**
 * A type switch is a chain of `instanceof` with two or more `instanceof`s.
 * This represents the first `if` statement in the chain.
 */
class TypeSwitchIofStmt extends ChainedIofStmt {
	TypeSwitchIofStmt() {
		n >= 2 and
		not exists(IfStmt other | this = other.getElse())
	}
	InstanceOfExpr getAnIof() {
		result = getElse*().(ChainedIofStmt).getIof()
	}
}

/**
 * A type switch cast is a cast inside a type switch chain.
 */
class TypeSwitchCast extends GuardByInstanceOfCast {
	TypeSwitchCast() {
		exists (TypeSwitchIofStmt ts | ts.getAnIof() = iof)
	}
}

/**
 * https://lgtm.com/projects/g/mockito/mockito/snapshot/da68900466a17e21fef3e27690f4cef4b5c240ea/files/src/main/java/com/android/volley/toolbox/HurlStack.java?sort=name&dir=ASC&mode=heatmap&showExcluded=false#L189
 * 
 * ```java
 * // use caller-provided custom SslSocketFactory, if any, for HTTPS
 * if ("https".equals(url.getProtocol()) && mSslSocketFactory != null) {
 *     ((HttpsURLConnection)connection).setSSLSocketFactory(mSslSocketFactory);
 * }
 * ```
 */
class SwitchFieldTypeTagCast extends Cast {
	FieldAccess tagAccess;
	FieldAccess castAccess;
	Variable v;
	SwitchFieldTypeTagCast() {
		tagAccess = this.(SwitchedExpr).getSwitchStmt().getExpr() and
		castAccess = getExprOrDef() and
		v.getAnAccess() = tagAccess.getQualifier() and
		v.getAnAccess() = castAccess.getQualifier()
	}
	FieldAccess getTagAccess() { result = tagAccess }
	FieldAccess getCastAccess() { result = castAccess }
	Variable getVariable() { result = v }
}

/**
 * A method that uses __static__ (known at compile-time) resources (outside source code) to create an object.
 */
abstract class StaticResourceMethodAccess extends MethodAccess {}

/**
 * Cast to `findViewById` (seen in Android applications).
 */
class FindViewByIdMethodAccess extends StaticResourceMethodAccess {
	FindViewByIdMethodAccess() {
		(
			getMethod().getDeclaringType().hasQualifiedName("android.view", "View") or
			getMethod().getDeclaringType().hasQualifiedName("android.app", "Activity") or
			getMethod().getDeclaringType().hasQualifiedName("android.app", "Dialog")
		) and
		getMethod().getName() = "findViewById" and
		getMethod().getNumberOfParameters() = 1 and
		getMethod().getParameterType(0).getName() = "int"
	}
}

/**
 * The `inflate` method access.
 */
class LayoutInflaterInflateCastTag extends StaticResourceMethodAccess {
  LayoutInflaterInflateCastTag() {
	  getMethod().getDeclaringType().hasQualifiedName("android.view", "LayoutInflater") and
	  getMethod().hasName("inflate")
  }
}

/**
 * Application class declared in AndroidManifest.xml
 */
class GetApplicationContextMethodAccess extends StaticResourceMethodAccess {
	GetApplicationContextMethodAccess() {
	  getMethod().getDeclaringType().hasQualifiedName("android.content", "Context") and
	  getMethod().hasName("getApplicationContext") and
	  getMethod().getNumberOfParameters() = 0
  }
}

/**
 * Cast to `android.content.res.Resources::getDrawable`.
 */
class ResourcesGetDrawableMethodAccess extends StaticResourceMethodAccess {
	ResourcesGetDrawableMethodAccess() {
		getMethod().getDeclaringType().hasQualifiedName("android.content.res", "Resources") and
		getMethod().hasName("getDrawable")
	}
}

/**
 * A cast to a method access to \code{findViewById} is a pattern in its own.
 */
class StaticResourceCast extends Cast {
	StaticResourceCast() {
		getExprOrDef() instanceof StaticResourceMethodAccess
	}
}

/**
 * 
 */
class SoleSubclassImplementation extends Cast {
	SoleSubclassImplementation() {
		count(RefType rt | 
				rt = getExpr().getType() and rt.fromSource() | 
				rt.getASubtype+() ) = 1
	}
}

class RemoveWildcardCast extends Cast {
	RemoveWildcardCast() {
		containsWildcard(getExpr().getType())
	}
}

/**
 *
 */
abstract class ReflectiveMethodAccess extends MethodAccess {}

/**
 * Represents a method access to the `invoke` method in defined the `java.lang.reflect.Method` type.
 * 
 * @see https://docs.oracle.com/javase/8/docs/api/java/lang/reflect/Method.html#invoke-java.lang.Object-java.lang.Object...-
 */
class MethodInvokeMethodAccess extends ReflectiveMethodAccess {
	MethodInvokeMethodAccess() {
		getMethod().hasName("invoke") and
		getMethod().getDeclaringType().hasQualifiedName("java.lang.reflect", "Method")
	}
}

/**
 * Represents a method access to the `get` method defined in the `java.lang.reflect.Field` type.
 * 
 * @see https://docs.oracle.com/javase/8/docs/api/java/lang/reflect/Field.html#get-java.lang.Object-
 */
class FieldGetMethodAccess extends ReflectiveMethodAccess {
	FieldGetMethodAccess() {
		getMethod().hasName("get") and
		getMethod().getDeclaringType().hasQualifiedName("java.lang.reflect", "Field")
	}
}

/**
 * Represents a method access to `java.lang.reflect.AccessibleObject.setAccessible`.
 * 
 * @see https://docs.oracle.com/javase/8/docs/api/java/lang/reflect/AccessibleObject.html
 */
class SetAccessibleMethodAccess extends MethodAccess {
	private Argument flagArgument;
	SetAccessibleMethodAccess() {
		getMethod().hasName("setAccessible") and
		getMethod().getDeclaringType().hasQualifiedName("java.lang.reflect", "AccessibleObject") and
		(
			(getNumArgument() = 1 and flagArgument = getArgument(0)) or
			(getNumArgument() = 2 and flagArgument = getArgument(1))
		)
	}
	
	/**
	 * Gets the `flag` argument to this `setAccessible` call site.
	 */
	Argument getFlagArgument() {
		result = flagArgument
	}
}

/**
 * A method access to `setAccessible` with `true` as argument, e.g., `f.setAccessible(true);`.
 */
class SetAccessibleTrueMethodAccess extends SetAccessibleMethodAccess {
	SetAccessibleTrueMethodAccess() {
		getFlagArgument().(BooleanLiteral).getBooleanValue() = true
	}
}

abstract class ReflectiveAccessibilityCast extends Cast {
	private ReflectiveMethodAccess reflectiveMethodAccess;
	Variable fieldVariable;
	SetAccessibleTrueMethodAccess satma;
	ReflectiveAccessibilityCast() {
		reflectiveMethodAccess = getExprOrDef() and
		fieldVariable.getAnAccess() = getExprOrDef().(MethodAccess).getQualifier().(VarAccess) and
		fieldVariable.getAnAccess() = satma.getQualifier()
	}
	ReflectiveMethodAccess getReflectiveMethodAccess() { result = reflectiveMethodAccess }
	Variable getFieldVariable() { result = fieldVariable }
	SetAccessibleTrueMethodAccess getSetAccessibilityTrueMethodAccess() { result = satma }
	abstract Expr getReflectiveMember();
	abstract Expr getReflectiveClass();
}

private module foreach {

	private final class VarNotNull extends NEExpr {
		Variable v;
		NullLiteral nil;
		VarNotNull() {
			 hasOperands(v.getAnAccess(), nil)
		}
		Variable v() { result = v }
	}

	private final class ConstEqualsComparison extends MethodAccess {
		ConstEqualsComparison() {
			getMethod() instanceof EqualsMethod and
			getArgument(0) instanceof CompileTimeConstantExpr
		}
	}

	private final class FieldGetNameMethodAccess extends MethodAccess {
		FieldGetNameMethodAccess() {
			getMethod().getName() = "getName" and
			getMethod().getNumberOfParameters() = 0
		}
	}

	private final class GetDeclaredFieldsMethodAccess extends MethodAccess {
		GetDeclaredFieldsMethodAccess() {
			getMethod().getName() = "getDeclaredFields" and
			getMethod().getNumberOfParameters() = 0
		}
	}
	
	/**
	 * https://lgtm.com/projects/g/loopj/android-async-http/snapshot/dist-1879340034-1539203194143/files/library/src/main/java/com/loopj/android/http/AsyncHttpClient.java?sort=name&dir=ASC&mode=heatmap&showExcluded=false#L445 
	 */
	class ForEachReflectiveAccessibilityCastPatern extends ReflectiveAccessibilityCast {
		private TypeLiteral cls;
		private ConditionBlock cbe;
		ForEachReflectiveAccessibilityCastPatern() {
			exists (ConditionBlock cb, VariableAssign assign, EnhancedForStmt for, LocalVariableDeclExpr vit, Variable fs |
			fieldVariable.getInitializer() instanceof NullLiteral and
			cb.getCondition().(VarNotNull).v() = fieldVariable and
			cb.controls(getBasicBlock(), true) and
			assign.getDestVar() = fieldVariable and
			vit = for.getVariable() and
			assign.getSource() = vit.getAnAccess() and
			cbe.controls(assign.getBasicBlock(), true) and
			cbe.getCondition().(ConstEqualsComparison).getQualifier().(FieldGetNameMethodAccess).getQualifier() = vit.getAnAccess() and
			fs.getAnAccess() = for.getExpr() and
			cls = fs.getInitializer().(GetDeclaredFieldsMethodAccess).getQualifier()
			)
		}
		override Expr getReflectiveMember() { result = cbe }
		override Expr getReflectiveClass() { result = cls }
	}

}

module declare {
	
	private final class GetDeclaredMethodMethodAccess extends MethodAccess {
		GetDeclaredMethodMethodAccess() {
			getMethod().getName() = "getDeclaredMethod"
		}
	}

	private final class DeclaredReflectiveAccessibilityCastPattern extends ReflectiveAccessibilityCast {
		private GetDeclaredMethodMethodAccess ma;
		private CompileTimeConstantExpr name;
		private TypeLiteral cls;
		DeclaredReflectiveAccessibilityCastPattern() {
			ma = fieldVariable.getInitializer() and
			name = ma.getArgument(0) and
			cls = ma.getQualifier()
		}
		override Expr getReflectiveMember() { result = name }
		override Expr getReflectiveClass() { result = cls }
	}
	
}

/**
 * 
 */
class RedundantCast extends Cast {
	RedundantCast() {
		getExpr().getType() = getTargetType() or (
			this instanceof UpCast and
			not this instanceof SelectOverloadCast and
			not this instanceof CovariantGenericCast
		)
	}
}

/**
 * 
 */
class ObjectAsArrayCast extends Cast {
	ArrayAccess arr;
	ObjectAsArrayCast() {
		arr = getExprOrDef() and
		arr.getIndexExpr() instanceof CompileTimeConstantExpr and
		arr.getArray().getType().(Array).
						getElementType() instanceof TypeObject
	}
	ArrayAccess getArrayAccess() { result = arr }
}

abstract class NewDynamicInstanceAccess extends MethodAccess {}

class NewInstanceAccess extends NewDynamicInstanceAccess {
	NewInstanceAccess() {
		getCallee().hasName("newInstance") and
		(
			getCallee().getDeclaringType() instanceof TypeClass or
			getCallee().getDeclaringType() instanceof TypeConstructor or
			getCallee().getDeclaringType() instanceof TypeArray
		)
	}
}

class NewProxyInstanceAccess extends NewDynamicInstanceAccess {
	NewProxyInstanceAccess() {
		getMethod().getDeclaringType().getQualifiedName()
						= "java.lang.reflect.Proxy" and
		getMethod().getName() = "newProxyInstance"
	}
}

/**
 * Represents a method access to a `newInstance` method defined in either
 * the `java.lang.Class`, the `java.lang.reflect.Constructor`, or the `java.lang.reflect.Array` types.
 * 
 * Notice that the class `NewInstance` is not used because does not include the `Array` type.
 * 
 * Cast to `java.lang.reflect.Proxy::newInstance`.
 * 
 * @see
 * https://docs.oracle.com/javase/8/docs/api/java/lang/Class.html#newInstance--
 * https://docs.oracle.com/javase/8/docs/api/java/lang/reflect/Constructor.html#newInstance-java.lang.Object...-
 * https://docs.oracle.com/javase/8/docs/api/java/lang/reflect/Array.html#newInstance-java.lang.Class-int...-
 * NewInstance
 */
class NewDynamicInstanceCast extends Cast {
	NewDynamicInstanceCast() {
		getExprOrDef() instanceof NewDynamicInstanceAccess
	}
}

/**
 * 
 */
final class LookupByIdCast extends Cast {
	private MethodAccess methodAccess;
	private Method getterMethod;
	private FieldAccess constantField;
	LookupByIdCast() {
		methodAccess = getExprOrDef() and
		getterMethod = methodAccess.getMethod() and
		not getterMethod.isStatic() and not getterMethod.isVarargs() and
		getterMethod.isPublic() and
		getterMethod.getNumberOfParameters() = 1 and
		getterMethod.getParameterType(0) instanceof TypeString and
		getterMethod.getReturnType() instanceof TypeObject and
		methodAccess.getArgument(0).getType() instanceof TypeString and
		methodAccess.getArgument(0) = constantField and
		constantField.getField().isFinal() and
		constantField.getField().isStatic() and
		constantField.getField().getType() instanceof TypeString
	}
	MethodAccess getMethodAccess() { result = methodAccess}
	Method getGetterMethod() { result = getterMethod }
	FieldAccess getConstantField() { result = constantField }
}

predicate notGenericRelated(Type type) {
	not type instanceof RawType and
	not type instanceof ParameterizedType and
	not type instanceof BoundedType
}

class ImplicitIntersectionTypeCast extends Cast {
	ImplicitIntersectionTypeCast() {
		getTargetType() instanceof Interface and
		not isSubtype(getTargetType(), getExpr().getType()) and
		not this instanceof UpCast and
		not getExpr() instanceof NullLiteral and
		notGenericRelated(getTargetType()) and
		notGenericRelated(getExpr().getType())
	}
}
