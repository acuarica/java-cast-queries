/**
 * This module provides facilities to pretty-print nodes in the AST.
 */

/*
 * using CastMethodAccess, ExplicitVarargsArrayArgument, VarargArgument, OverloadedArgument
 */
private import casts

/*
 * using defUsePair, import java
 */
private import semmle.code.java.dataflow.DefUse

/**
 * Pretty-prints the observation. It aids to analyze all observations.
 */
abstract class Show extends Top {
	abstract string show();
}

/**
 * Provides classes for pretty-printing with Java expressions.
 */
private module expr {
		
	/**
	 * Shows an expression resorting to the `toString` predicate.
	 */
	class ShowExpr extends Show, Expr {
		override string show() {
			result = this.(Expr).toString() + ":-" + getType().(Show).show()
		}
	}

	/**
	 * Shows a string literal.
	 * It changes the double quotes to two single quotes.
	 * Otherwise it is not possible to parse the CSV output due to conflicting double quotes.
	 */
	final class ShowStringLiteral extends ShowExpr, StringLiteral {
		override string show() {
			result = "''" + getRepresentedString() + "''"
		}
	}
	
	/**
	 * Shows a cast expression. This is the focus of our study.
	 */
	final class ShowCastExpr extends ShowExpr, CastExpr {
		override string show() {
			result = "(" + getTypeExpr().(Show).show() + ") " + getExpr().(Show).show() + " ↵ " + getParent().(Show).show() + " ⊆ " + getEnclosingCallable().(Show).show()
		}
	}

	/**
	 * Shows a `new` expression.
	 */
	final class ShowClassInstanceExpr extends ShowExpr, ClassInstanceExpr {
		override string show() {
			result = this.toString() + ":" + getType().(Show).show()
		}
	}

	/**
	 * Shows a `null` literal expression.
	 */
	final class ShowNullLiteral extends ShowExpr, NullLiteral {
		override string show() {
			result = this.(NullLiteral).toString() + ": " + getType()
		}
	}
	
	/**
	 * Shows an explicit varargs array argument.
	 */
	final class ShowExplicitVarargsArrayArgument extends Show, CastExpr, ExplicitVarargsArrayArgument {
		override string show() {
			result = "ex varargs[...]" + param + ":" + param.getType()
		}
	}

	/**
	 * Shows a Vararg argument.
	 */
	final class ShowVarargArgument extends Show, CastExpr, VarargArgument {
		override string show() {
			result = "varArg[]" + param
		}
	}
	
	/**
	 * Shows an overloaded argument.
	 */
	final class ShowOverloadedArgument extends Show, CastExpr, OverloadedArgument {
		override string show() {
			result = "overloaded: " + param + "-" + getTarget() + "~" + count(getAnOverload())
		}
	}

	/**
	 * Shows a callable.
	 */
	final class ShowCallable extends Show, Callable {
		override string show() {
			result = concat("@" + getAnAnnotation(), ",") + getBody().(Show).show() + " " + this.(Callable).toString()
		}
	}

	/**
	 * Shows a variable.
	 */
	final class ShowVariable extends Show, Variable {
		override string show() {
			result = concat("@" + getAnAnnotation(),  ",") + getName() + ": " + getType().(Show).show()
		}
	}

	/**
	 * Show a variable access, and a Def/Use if any.
	 */
	final class ShowVarAccess extends ShowExpr, VarAccess {
		override string show() {
			result = this.(VarAccess).toString() + "`var: " + getType() + "=" + showDefUse()
		}
		private string showDefUse() {
			exists (VariableAssign def | 
				defUsePair(def, this) and
				result = def.getSource().(Show).show()
			) or
			(not exists (VariableAssign def | defUsePair(def, this)) and result = "✻")
		}
	}

	/**
	 * Shows a local variable declaration.
	 */
	final class ShowLocalVariableDeclExpr extends ShowExpr, LocalVariableDeclExpr {
		override string show() {
			result = "decl " + getVariable().(Show).show() + "=" + getInit().toString()
		}
	}
	
	/**
	 * Shows a method access.
	 */
	class ShowMethodAccess extends ShowExpr, MethodAccess {
		override string show() {
			result = showQualifier() + showName() + showStatic() + "(..): " + getType().toString()
		}
		private string showQualifier() {
			if exists(getQualifier()) then result = getQualifier() + "." else result = ""
		}
		private string showName() {
			if exists(getMethod().toString()) then result = getMethod().toString() else result = "?UnknownMethod?"
		}
		private string showStatic() {
			if getMethod().isStatic() then result = "`static" else result = ""
		}
	}
	
	/**
	 * Shows an array access, i.e., `array[i]`.
	 */
	final class ShowArrayAccess extends Show, ArrayAccess {
		override string show() {
			result = "ArrayAccess {array=" + getArray() + ", indexExpr=" + getIndexExpr() + "}"
		}
	}
	
	/**
	 * Shows a type used as an expression.
	 */
	final class ShowTypeAccess extends ShowExpr, TypeAccess {
		override string show() {
			result = ":" + getType() + "!" + getType().(Show).show()
		}
	}

	/**
	 * Shows an array type used as an expression.
	 */
	final class ShowArrayTypeAccess extends ShowExpr, ArrayTypeAccess {
		override string show() {
			result = getComponentName().(Show).show() + "[]"
		}
	}

}

/**
 * Module to shows types.
 */
private module type {

	/**
	 * Shows a type using the `toString` predicate.
	 */
	class ShowType extends Show, Type {
		override string show() {
			result = this.(Type).toString()
		}
	}

	/**
	 * Shows a reference type, including all its supertypes.
	 */
	class ShowRefType extends ShowType, RefType {
		override string show() {
			result = this.(RefType).toString() + "<:" + concat (getASupertype().toString(), ",")
		}
	}
	
	/**
	 * Shows a parameterized type together with its type argument.
	 */
	final class ShowParameterized extends ShowRefType, ParameterizedType {
		override string show() {
			result = "Param%" + getATypeArgument().(Show).show()
		}
	}

	/**
	 * Shows a generic type.
	 */
	final class ShowGenericType extends ShowRefType, GenericType {
		override string show() {
			result = "Generic"
		}
	}

	/**
	 * Shows a type variable.
	 */
	final class ShowTypeVariable extends ShowRefType, TypeVariable {
		override string show() {
			result = "TypeVar=" + getName()
		}
	}

	/**
	 * Shows a wildcard.
	 */
	final class ShowWildcard extends ShowRefType, Wildcard {
		override string show() {
			result = "Wildcard"
		}
	}

}

/**
 * Shows different kinds of annotations.
 */
private module annotation {
	
	/**
	 * Shows a base annotation.
	 * It just adds the symbol `@` to the `toString` representation.
	 */
	final class ShowAnnotation extends expr::ShowExpr, Annotation {
		override string show() {
			result = "@" + toString()
		}
	}

	/**
	 * Shows a test annotation.
	 */
	final class ShowTestAnnotation extends expr::ShowExpr, TestAnnotation {
		override string show() {
			result = "🗒" + toString()
		}
	}

	/**
	 * Shows a `SuppressWarnings` annotation.
	 */
	final class ShowSuppressWarningsAnnotation extends expr::ShowExpr, SuppressWarningsAnnotation {
		override string show() {
			result = "🗒SuppressWarnings(" + getASuppressedWarning() + ")"
		}
	}

}

/**
 * Shows statements.
 */
private module statement {

	/**
	 * Shows a statement resorting to `toString`.
	 */
	class ShowStmt extends Show, Stmt {
		override string show() {
			result = this.(Stmt).toString()
		}
	}

	/**
	 * Whether this block statement contains a single statement. 
	 */
	final class ShowSingletonBlock extends ShowStmt, SingletonBlock {
		override string show() {
			result = "1️⃣"
		}
	}

	/**
	 * Shows a `return` statement.
	 * It uses `toString` to display the returning expression --- instead of `show` ---
	 * because otherwise causes an infinite recursion.
	 */
	final class ShowReturnStmt extends ShowStmt, ReturnStmt {
		override string show() {
			result = "return " + getResult().toString()
		}
	}

	/**
	 * Shows a `try` statement.
	 */
	final class ShowTryStmt extends ShowStmt, TryStmt {
		override string show() {
			result = "try {" + getBlock().toString() + "}"
		}
	}
	
	/**
	 * Shows a `throw` statement.
	 * It uses `toString` to display the throwing expression --- instead of `show` ---
	 * because otherwise causes an infinite recursion.
	 */
	final class ShowThrowStmt extends ShowStmt, ThrowStmt {
		override string show() {
			result = "throw " + getExpr().toString()
		}
	}

}
