import casts

bindingset[suffix]
string getAClass(Top obs, string suffix) {
	exists (string qlclass | qlclass = obs.getAQlClass() and
		qlclass.suffix(qlclass.length() - suffix.length()) = suffix and
		result = qlclass.prefix(qlclass.length() - suffix.length())
	)
}

/**
 * The category to which this observation belongs to.
 */
abstract class Category extends string {
	bindingset[this]
	Category() { any() }
	abstract Top getAnObs();
}

final class CastCategory extends Category {
	CastCategory() { this = "Cast" }
	override Top getAnObs() { result instanceof Cast }
}

final class CastPatternsCategory extends Category {
	Cast cast;
	CastPatternsCategory() { this = getAClass(cast, "Cast") }
	override Top getAnObs() { result = cast }
}
