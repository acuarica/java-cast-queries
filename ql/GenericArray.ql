import lib.casts

query predicate a(OnArrayGenericArrayCast c) { any() }
query predicate b(TypeVariableGenericArrayCast c) { any() }
query predicate c(OnElementGenericArrayCast c) { any() }
