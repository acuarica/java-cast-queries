import lib.casts

from CovariantGenericCast c
select
	c,
	c.getArgument(),
	c.getArgument().getPosition(),
	c.getArgument().getType(), c.getCallSite(),
	c.getCallee(),
	c.getCallee().getReturnType(),
	c.getCallee().getParameterType(c.getArgument().getPosition())
