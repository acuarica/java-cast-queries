import lib.casts

from RedundantCast c
select
	c,
	c.(CastExpr).getExpr().getType(),
	c.getTargetType()
