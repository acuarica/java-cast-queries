import java

select count(Method m | exists(Block b | m.getBody() = b))
