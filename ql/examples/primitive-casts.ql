import java

class PrimitiveCast extends CastExpr {
	PrimitiveCast() {
		getExpr().getType() instanceof PrimitiveType and
		getTypeExpr().getType() instanceof PrimitiveType
	}
}

from PrimitiveCast ce
select ce
