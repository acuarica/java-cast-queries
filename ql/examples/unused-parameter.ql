import java

from Parameter p, Method m
where not exists(p.getAnAccess())
  and m.getAParameter() = p
  and not m.isAbstract()
select p, m
