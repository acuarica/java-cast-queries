import lib.casts

from ReflectiveAccessibilityCast c
select
	c,
	c.getReflectiveMethodAccess(),
	c.getFieldVariable(),
	c.getSetAccessibilityTrueMethodAccess(),
	c.getReflectiveMember(),
	c.getReflectiveClass()
