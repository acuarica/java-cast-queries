import lib.casts	

from LookupByIdCast ce
select
	ce,
	ce.getMethodAccess(),
	ce.getGetterMethod(),
	ce.getConstantField(),
	ce.getConstantField().getField()
