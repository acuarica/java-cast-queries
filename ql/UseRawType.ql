import lib.casts

from UseRawTypeCast c
select
	c,
	c.getMethodAccess().getQualifier(),
	c.getMethodAccess().getMethod(),
	c.getMethodAccess().getMethod().getReturnType(),
	c.getMethodAccess().getMethod().getSourceDeclaration(),
	c.getMethodAccess().getMethod().getSourceDeclaration().getReturnType()
