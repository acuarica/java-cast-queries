import lib.casts

from ImplicitIntersectionTypeCast c
select c, c.getTargetType(), c.getExpr().getType()
