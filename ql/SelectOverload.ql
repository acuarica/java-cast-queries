import lib.casts

from SelectOverloadCast c
select c, c.getType(), c.getExpr(), c.getExpr().getType(), c.getOverload()
