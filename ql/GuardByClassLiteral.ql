import lib.casts

from GuardByClassLiteral c
select
	c,
	c.getTargetType(),
	c.getClassLiteral(),
	c.getClassLiteral().getType(),
	c.getClassLiteral().getTypeName(),
	c.getClassLiteral().getTypeName().getType(),
	c.getGetClass()
