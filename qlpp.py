
import sys
import os

qlFile = sys.argv[1]
basedir = os.path.dirname(qlFile)
# print(basedir)
# project, _ = os.path.splitext(filename)
# print(project)

def importQll(qllModule, qllModules):
    qlFile = basedir + "/" + qllModule + ".qll"
    if not qllModule in qllModules:
        qllModules.append(qllModule)
        if os.path.exists(qlFile):
            pp(qlFile, qllModules)
        else:
            print("import " + qllModule)

def pp(qlFile, qllModules):
    with open(qlFile, "r") as f:
        for line in f.readlines():
            line = line.rstrip()
            words = line.split()
            if len(words) >= 2 and words[0] == "import":
                importQll(words[1], qllModules)
            elif len(words) >= 3 and words[0] == "private" and words[1] == "import":
                importQll(words[2], qllModules)
            elif line.startswith("query predicate"):
                line = line.replace("query predicate", "predicate")
                print(line)
            else:
                print(line)

qllModules = []
pp(qlFile, qllModules)
