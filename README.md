
# Java Cast Queries

## Introduction

Here we have written QL queries for our cast study.
These queries are located in the `ql` folder.
You can find in each query a description of the pattern it detects.

QL Queries for lgtm.com

## query-results

### Import

Total files/repos imported: 7.559
Total casts rows imported: 10.193.435
Total nonSnapshots: 215

Total casts in non-snapshots: 1.162.583
Total non-snapshots w/casts: 203

Total repos w/casts: 7.043
Total repos w/no casts: 516

Total casts in snapshots: 9.030.852
Total snapshots w/casts: 6.840

Sample size: 10000-incomplete

Contradictory: https://lgtm.com/rules/5970069/